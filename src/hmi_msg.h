/*This file is part of hardware.

    hardware is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    hardware is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with hardware.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_
#include <avr/pgmspace.h>
#define UPTIME "Uptime: %lu s"
#define STUD_NAME "Martin Arusalu"
#define VER_FW "Version: %S built on: %S %S"
#define VER_LIBC "avr-libc version: %S avr-gcc version : %S"

#define HELP_CMD "help"
#define HELP_HELP "Get help"
#define VER_CMD "version"
#define VER_HELP "Print FW version"
#define ASCII_CMD "ascii"
#define ASCII_HELP "print ASCII tables"
#define MONTH_CMD "month"
#define MONTH_HELP "Find matching month from lookup list. Usage: month <string>"
#define CLI_HELP_MSG "Implemented commands:"
#define CLI_NO_CMD "Command not implemented.\n Use <help> to get help."
#define CLI_ARGS_MSG "To few or to many arguments for this command\nUse <help>"
#define READ_CMD "read"
#define READ_HELP "Read UID from RFID card"
#define ADD_CMD "add"
#define ADD_HELP "Add card to memory by name (card needs to be in contact)"
#define REMOVE_CMD "remove"
#define REMOVE_HELP "Remove card from memory by name"
#define LIST_CMD "list"
#define LIST_HELP "List all valid cards"
#define HELP1 "Implemented commands:\n"
#define HELP2 " : "
#define ACCESS "ACCESS DENIED!"

extern PGM_P const month_lst[];

extern const char help_cmd[];
extern const char help_help[];
extern const char ver_cmd[];
extern const char ver_help[];
extern const char ascii_cmd[];
extern const char ascii_help[];
extern const char month_cmd[];
extern const char month_help[];
extern const char read_cmd[];
extern const char read_help[];
extern const char add_cmd[];
extern const char add_help[];
extern const char remove_cmd[];
extern const char remove_help[];
extern const char list_cmd[];
extern const char list_help[];


#endif /* _HMI_MSG_H_ */
