/*This file is part of hardware.

    hardware is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    hardware is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with hardware.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include "hmi_msg.h"
#include "uart-wrapper.h"
#include "print_helper.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "cli_microrl.h"
#include "rfid.h"
#include "../lib/matejx_avr_lib/mfrc522.h"

volatile uint32_t seconds;

int in_list;
int done;

static microrl_t rl;
static microrl_t *prl = &rl;


static inline void init_rfid_reader(void)
{
    MFRC522_init();
    PCD_Init();
}


static inline void init_clock(void)
{
    TCCR5A = 0;
    TCCR5B = 0;
    TCCR5B |= _BV(WGM52) | _BV(CS52);
    OCR5A = 62549;
    TIMSK5 |= _BV(OCIE5A);
}


static inline void init_hw (void)
{
    DDRA |= _BV(DDA3);
    init_clock();
    uart0_init(UART_BAUD_SELECT(9600, F_CPU));
    uart3_init(UART_BAUD_SELECT(9600, F_CPU));
    stdout = stdin = &uart0_io;
    stderr = &uart3_out;
    lcd_init();
    lcd_clrscr();
    sei();
}


static inline void print_info (void)
{
    print_versions(stderr);
    fprintf_P(stdout, PSTR(STUD_NAME));
    fputc('\n', stdout);
    lcd_puts_P(PSTR(STUD_NAME));
}


static inline void cli_init (void)
{
    microrl_init (prl, cli_print);
    microrl_set_execute_callback (prl, cli_execute);
}


static inline void heartbeat (void)
{
    static uint32_t time;
    uint32_t elapsed = 0;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        elapsed = seconds;
    }

    if ((time - elapsed) > 0) {
        PORTA ^= _BV(PORTA3);
        fprintf_P(stderr, PSTR(UPTIME "\n"), elapsed);
    }

    time = elapsed;
}


void check(void)
{
    static uint32_t since_allowed;
    static uint32_t since_blocked;

    if (PICC_IsNewCardPresent()) {
        int found = 0;

        if (head != NULL) {
            card_t *current;
            current = head;

            while (current != NULL) {
                Uid card_uid;
                Uid *uid_ptra = &card_uid;
                PICC_ReadCardSerial(uid_ptra);

                if (!memcmp(card_uid.uidByte, current->uid, card_uid.size)) {
                    in_list = 1;
                    PORTA |= _BV(PORTA1);
                    since_allowed = seconds;
                    lcd_clrscr();
                    lcd_puts_P(PSTR(STUD_NAME));
                    lcd_goto(0x40);
                    lcd_puts(current->name);
                    done = 0;
                    found = 1;
                }

                current = current->next;
            }
        }

        if (head == NULL || found == 0) {
            in_list = 0;
            PORTA &= ~_BV(PORTA1);
            since_blocked = seconds;
            lcd_clrscr();
            lcd_puts_P(PSTR(STUD_NAME));
            lcd_goto(0x40);
            lcd_puts_P(PSTR(ACCESS));
            done = 0;
        }

        in_list = found;
    }

    if (in_list == 1 && (seconds - since_allowed) < 3) {
        PORTA |= _BV(PORTA1);
    } else {
        PORTA &= ~_BV(PORTA1);
    }

    if (((seconds - since_blocked) >= 7) && ((seconds - since_allowed) >= 7) &&
            done == 0) {
        lcd_clrscr();
        lcd_puts_P(PSTR(STUD_NAME));
        done = 1;
    }
}


int main (void)
{
    init_rfid_reader();
    init_hw();
    print_info();
    cli_init();
    done = 1;

    while (1) {
        heartbeat();
        microrl_insert_char (prl, cli_get_char());
        check();
    }
}


ISR(TIMER5_COMPA_vect)
{
    seconds++;
}
